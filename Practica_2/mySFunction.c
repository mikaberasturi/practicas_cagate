/*
 * File: 
 * Abstract:
 *       Control del lazo interno de corriente para el convertidor del lado
 *       del rotor (RSC) del DFIG. 
 *
 *      Parametros de entrada a la S-Function:
 *      Ts, P, Ls, Lm, Lr', Kp, Ti, b y Vrp
 *
 *      Entradas:
 *      U0(0) : vsA
 *      U0(1) : vsB
 *      U0(2) : ira
 *      U0(3) : irb
 *      U0(4) : theta_rm
 *      U0(5) : omega_rm
 *      U0(6) : fs
 *      U0(7) : irx_ref
 *      U0(8) : iry_ref
 *
 *      Salidas: 
 *		y0[0] : vra
 *		y0[1] : vrb
 *		y0[2] : vrc
 *      y0[3] : irx
 *      y0[4] : iry
 *
 *      Estados discretos:
 *      xd[0]: vhatrx_k_1
 *      xd[1]: irxref_k_1
 *      xd[2]: irx_k_1
 *      xd[3]: vhatry_k_1
 *      xd[4]: iryref_k_1
 *      xd[5]: iry_k_1
 *
 *      Parametros y variables internas necesarias: (Son las Rworks, las que quiero compartir o enviar a actualizar)
 *      rwork[0] : vra
 *      rwork[1] : vrb
 *      rwork[2] : vrc
 *      rwork[3] : irx
 *      rwork[4] : iry
 *      rwork[5] : Ts
 *      rwork[6] : P
 *      rwork[7] : Ls
 *      rwork[8] : Lm
 *      rwork[9] : Lr'
 *      rwork[10]: Kp
 *      rwork[11]: Ti
 *      rwork[12]: b
 *		rwork[13]: Vrp
 */

# define S_FUNCTION_NAME mySFunction
# define S_FUNCTION_LEVEL 2



# include "simstruc.h"
# include <math.h>

# define U0(element)	(*uPtrs0[element])  /* Puerto de entrada 0 */
# define PI             3.141592653589793
# define SQRT3          1.73205080756888

////////////////////////////////////////////////////////////////
// Definici�n de los �ndices de entradas, salidas, rworks, etc.
// #define genera una macro para que se pueda sustituir 
// Entradas:
# define vsA			0
# define vsB			1
# define ira			2
# define irb			3
# define theta_rm		4
# define omega_rm		5
# define fs				6
# define irx_ref		7
# define iry_ref		8

// Salidas (�ndices compartidos con los 5 primeros rworks)
# define vra			0
# define vrb			1
# define vrc			2
# define irx			3
# define iry			4

// Estados discretos
# define vhatrx_k_1		0 // Tension hat o con sombrero rx en k-1
# define irxref_k_1		1
# define irx_k_1		2
# define vhatry_k_1		3
# define iryref_k_1		4
# define iry_k_1		5

// Par�metros y variables inernas
# define Ts				5
# define P				6
# define Ls				7
# define Lm				8
# define Lrp			9
# define Kp				10
# define Ti				11
# define b				12
# define Vrp			13
////////////////////////////////////////////////////////////////

static void mdlInitializeSizes(SimStruct *S)
{

	ssSetNumSFcnParams(S, 9);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) { // Compruebo si le paso nueve parametros
        return;
    }

    ssSetNumContStates(S, 0);   /* number of continuous states           */
    ssSetNumDiscStates(S, 6);   /* number of discrete states             */
    // �Cuantos puertos de entrada? para empezar 1
    if (!ssSetNumInputPorts(S, 1)) return;    
    // �Que anchura tiene?
    ssSetInputPortWidth(S, 0, 9);    // el PUERTO 0 tiene 9 entradas
    // �Este puerto 0 esta relacionado con las entradas? NO
    ssSetInputPortDirectFeedThrough(S, 0, 0);

    //Puertos de salida
    //�Cuantos?
    if (!ssSetNumOutputPorts(S, 1)) return;
    // �Ese puerto de salida que anchura va a tener?
    ssSetOutputPortWidth(S, 0, 5);
    // Pasar como parametros el unico periodo de muestreo
    ssSetNumSampleTimes(S, 0);   /* number of sample times                */

    // Vamos a usar numeros enteros y reales
    // Necesitamos que el vector de R(real)Work tenga 14 elemenos 
    // En el examen nos dan codigo con huecos en los parentesis para ver que tenemos algo de idea
    ssSetNumRWork(S, 14);   /* number of real work vector elements   */
    ssSetNumIWork(S, 0);   /* number of integer work vector elements*/
    ssSetNumPWork(S, 0);   /* number of pointer work vector elements*/

    /* Specify the sim state compliance to be same as a built-in block */
    /* see sfun_simstate.c for example of other possible settings */
    //ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
    //ssSetOptions(S, 0);   /* general options (SS_OPTION_xx)        */

} /* end mdlInitializeSizes */


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *
 *    This function is used to specify the sample time(s) for your S-function.
 *    You must register the same number of sample times as specified in
 *    ssSetNumSampleTimes. If you specify that you have no sample times, then
 *    the S-function is assumed to have one inherited sample time.
 *
 *    To check for a sample hit during execution (in mdlOutputs or mdlUpdate),
 *    you should use the ssIsSampleHit or ssIsContinuousTask macros.
 *    For example, if your first sample time is continuous, then you
 *    used the following code-fragment to check for a sample hit. Note,
 *    you would get incorrect results if you used ssIsSampleHit(S,0,tid).
 *        if (ssIsContinuousTask(S,tid)) {
 *        }
 *    If say, you wanted to determine if the third (discrete) task has a hit,
 *    then you would use the following code-fragment:
 *        if (ssIsSampleHit(S,2,tid) {
 *        }
 *
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    /* Register one pair for each sample time */
    //ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    //ssSetOffsetTime(S, 0, 0.0);
    
    // Aqui se dimensiona el periodo de muestreo, no le metemos offset
    ssSetSampleTime(S, 0, (real_T) *mxGetPr(ssGetSFcnParam(S, 0))); // El periodo de muestreo
    //Mediante ssGetFcnParameter, pedimos y recupera el valor de la variable, devolviendote un puntero que apunta a la direccion de esaa variable
    ssSetOffsetTime(S, 0, 0.0);

} /* end mdlInitializeSampleTimes */


#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
  /* Function: mdlStart =======================================================
   * Abstract:
   *    This function is called once at start of model execution. If you
   *    have states that should be initialized once, this is the place
   *    to do it.
   */
  static void mdlStart(SimStruct *S) // Solo se ejecuta una vez al inicio!!!!!!
  {     
     // Obtener el puntero al vector de estados discretos
     real_T *xd = ssGetDiscStates(S); // xd son los estados discretos
     
      // Obtener el puntero al vector de variables reales
     real_T *rwork=ssGetRWork(S);

     unsigned char i;

     // Quiero recuperar los parametros que le estamos pasadno a la SFunction
     // Recuperacion de parametros
     for (i = 0; i < 9; i++)
     {
         rwork[i + 5] = (real_T)*mxGetPr(ssGetSFcnParam(S, i));
     }
     // Bucle para inicializar a 0
     for (i = 0; i < 6; i++)
     {
         xd[i] = 0.0;
     }
     // Inicializar los Rworks que contienen los valores a sacar lpor las salidias a 0
     for (i = 0; i < 5; i++)
     {
         // Posicion de 0 - 4
         rwork[i] = 0.0;
     }

  }
#endif /*  MDL_START */


static void mdlOutputs(SimStruct *S, int_T tid)
{   
    // Aqui sacamos las se�ales hacia fuera
    // Obterner el puntero al vector de salidas del puerto 0
    real_T *y0 = ssGetOutputPortRealSignal(S,0);
    
     // Obtener el puntero al vector de variables reales
    real_T *rwork=ssGetRWork(S);
    
    // Si ha pasado el periodo de muestreo
     if (ssIsSampleHit(S,0,tid))
     { 
        // Un �nico periodo de muestreo
		y0[vra] = rwork[vra]; // Actualizamos la salida
        y0[vrb] = rwork[vrb];
        y0[vrc] = rwork[vrc];
        y0[irx] = rwork[irx];
        y0[iry] = rwork[iry];
        // Ya hemos actualizado las salidas
     }
    
    
} /* end mdlOutputs */


# define MDL_UPDATE  /* Change to #undef to remove function */
# if defined(MDL_UPDATE)
  /* Function: mdlUpdate ======================================================
   * Abstract:
   *    This function is called once for every major integration time step.
   *    Discrete states are typically updated here, but this function is useful
   *    for performing any tasks that should only take place once per
   *    integration step.
   */
  static void mdlUpdate(SimStruct *S, int_T tid) // Aqui se implementa todo el control
  {
      // Obtener el puntero a la direcci�n del vector de entradas del puerto 0
	  // (doble puntero)
     InputRealPtrsType uPtrs0 = ssGetInputPortRealSignalPtrs(S,0); // Es como llamar las entradas  U()
     
     // Obtener el puntero al vector de estados discretos
     real_T *xd = ssGetDiscStates(S);    
     
     // Obtener el puntero al vector de variables reales
     real_T *rwork = ssGetRWork(S);
     
     real_T vsD, vsQ, vralpha, vrbeta, iralpha, irbeta, theta_vs, wls;
     real_T theta_xy, theta_r, vhatrx, vhatry, vdrx, vdry, vrx, vry, wms, wsl, mod_vs, mod_vr;
     // Si ha pasado el periodo de muestreo
     if (ssIsSampleHit(S, 0, tid)) // en un unico periodo de muestreo
     {
		// Muy parecido a implememntar en un micro
        // Paso 1 y 2. Recuperar se�ales de entrada y transformaciones
        vsD = U0(vsA); // Accedo a la tension de estator de la fase A
        vsQ = (U0(vsA) + 2 * U0(vsB)) / SQRT3;
        iralpha = U0(ira); // Accedo a la tension de estator de la fase A
        irbeta = (U0(ira) + 2 * U0(irb)) / SQRT3;

        // Paso 3. C�lculo del �ngulo theta_xy
        theta_vs = atan2(vsQ, vsD);
        theta_xy = theta_vs - PI / 2;

        // Paso 4. Calculo de irx e iry
        theta_r = rwork[P] * U0(theta_rm);

        rwork[irx] = iralpha * cos(theta_xy - theta_r) + irbeta * sin(theta_xy - theta_r);
        rwork[iry] = -iralpha * sin(theta_xy - theta_r) + irbeta * cos(theta_xy - theta_r);

        // Paso 5. Se�ales de control generadas por los I-Ps
        vhatrx = xd[vhatrx_k_1]
            + rwork[Kp] * (rwork[b] + rwork[Ts] / (2 * rwork[Ti])) * U0(irx_ref)
            - rwork[Kp] * (1 + rwork[Ts] / (2 * rwork[Ti])) * rwork[irx]
            + rwork[Kp] * (rwork[Ts] / (2 * rwork[Ti]) - rwork[b]) * xd[irxref_k_1]
            - rwork[Kp] * (rwork[Ts] / (2 * rwork[Ti]) - 1) * xd[irx_k_1];
        vhatry = xd[vhatry_k_1]
            + rwork[Kp] * (rwork[b] + rwork[Ts] / (2 * rwork[Ti])) * U0(iry_ref)
            - rwork[Kp] * (1 + rwork[Ts] / (2 * rwork[Ti])) * rwork[iry]
            + rwork[Kp] * (rwork[Ts] / (2 * rwork[Ti]) - rwork[b]) * xd[iryref_k_1]
            - rwork[Kp] * (rwork[Ts] / (2 * rwork[Ti]) - 1) * xd[iry_k_1];

        // Paso 6. C�lculo de los terminos de desacoplo
        //Frecuencia de sincronismo 
        wms = 2 * PI * U0(fs);
        wsl = wms - rwork[P] * U0(omega_rm);
        mod_vs = sqrt(vsD * vsD + vsQ * vsQ);

        vdrx = -wls * rwork[Lrp] * rwork[iry];
        vdry = wls * (rwork[Lm] * mod_vs / (rwork[Ls] * wms) + rwork[Lrp] * rwork[irx]);

        // Paso 7. Obtencion de las tensiones rotoricas en x e y
        vrx = vhatrx + vdrx;
        vry = vhatry + vdry;
        mod_vr = sqrt(vrx * vrx + vry * vry);

        if (mod_vr > rwork[Vrp])
            // Limitamos en caso de ser mayor que la tension que soporta
        {
            vrx = rwork[Vrp] * vrx / mod_vr;
            vry = rwork[Vrp] * vry / mod_vr;
            // A la tension que generan los PIs hay quer limitarla, �como?. Pues eliminando a las tensiones de cada eje
            // los terminos de acoplo. De este modo somos capaces de actuar directamente sobre la se�al del Pi sin acoplo y poder manipularla.
            vhatrx = vrx - vdrx;
            vhatry = vry - vdry;
            // Ahora ya se puede limitar la respuesta(commo un ANTIWind-UP)
        }
      
        // Actualizar estados discretos antes de hacer las conversiones mediante Clarke y Park
        xd[vhatrx_k_1] = vhatrx;
        xd[irxref_k_1] = U0(irx_ref);
        xd[irx_k_1] = rwork[irx];
        xd[vhatry_k_1] = vhatry;
        xd[iryref_k_1] = U0(iry_ref);
        xd[iry_k_1] = rwork[iry];

        // Paso 8. Transformacion de Park
        vralpha = vrx * cos(theta_xy - theta_r) - vry * sin(theta_xy - theta_r);// Los angulos estan en radianes
        vrbeta = vrx * sin(theta_xy - theta_r) + vry * cos(theta_xy - theta_r);

        // Paso 9. Transformada inversa de Clarke
        rwork[vra] = vralpha;
        rwork[vrb] = 0.5 * (-vralpha + SQRT3 * vrbeta);
        rwork[vrb] = -0.5 * (vralpha + SQRT3 * vrbeta);



     }
  }
# endif /* MDL_UPDATE */


# undef MDL_DERIVATIVES  /* Change to #undef to remove function */
# if defined(MDL_DERIVATIVES)
  /* Function: mdlDerivatives =================================================
   * Abstract:
   *    In this function, you compute the S-function block's derivatives.
   *    The derivatives are placed in the derivative vector, ssGetdX(S).
   */
  static void mdlDerivatives(SimStruct *S)
  {
  }
# endif /* MDL_DERIVATIVES */


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was allocated
 *    in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
}

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
